package asyncTest

import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue

class Hub {
    val truckQueue: BlockingQueue<Truck> = ArrayBlockingQueue(5)

    private fun unload(truck: Truck): Goods {
        val unloadTime = truck.capacity * 200
        Thread.sleep(unloadTime.toLong())
        return Goods(truck.capacity * 10)
    }

    fun work() {
        val createTruckThread = Thread {
            while (true) {
                truckQueue.put(TruckGenerator.nextTruck())
                println("Truck created")
            }
        }
        createTruckThread.start()

        val unloadThread = Thread {
            while (true) {
                val goods = unload(truckQueue.take())
                println("Truck unload  $goods")
            }
        }
        unloadThread.start()
    }
}