package asyncTest

import java.util.*

object TruckGenerator {
    fun nextTruck(): Truck {
        Thread.sleep((Random().
                nextInt(1500) + 500.toLong()))
        return Truck(Random().nextDouble() * 20 + 10)
    }
}